package binaryTree;

public class Realization<K,V> {
    BinaryTree<K, V> langList[] = new BinaryTree[100];

    public V langGet(K key) {
        int index = getHash(key);
        BinaryTree<K, V> list = langList[index];
        return getMatchValue(list, key);
    }

    public void langPut(K key, V value) {
        int index = getHash(key);
        storeValue(index, key, value);
    }

    public void langRemove(K key) {
        int index = getHash(key);
        BinaryTree<K, V> list = langList[index];
        if (list == null)
            return;
        if (list.getKey().equals(key)) {
            if (list.next == null) {
                langList[index] = null;
                return;
            }
        }
        BinaryTree<K, V> prev = null;
        do {
            if (list.key.equals(key)) {
                if (prev == null) {
                    list = list.getNext();
                } else {
                    prev.next = list.getNext();
                }
                break;
            }
            list = list.next;
        } while (list != null);

        langList[index] = list;
    }


    private V getMatchValue(BinaryTree<K, V> list, K key) {
        while (list != null) {
            if (list.getKey().equals(key))
                return list.getValue();
            list = list.next;
        }
        return null;
    }


    private void storeValue(int index, K key, V value) {
        BinaryTree<K, V> list = langList[index];
        if (list == null) {
            langList[index] = new BinaryTree<K, V>(key, value);
        } else {
            boolean done = false;
            while (list.next != null) {
                if (list.getKey().equals(key)) {
                    list.setValue(value);
                    done = true;
                    break;
                }
                list = list.next;
            }
            if (!done)
                list.next = new BinaryTree<K, V>(key, value);
        }

    }

    private int getHash(K key) {
        int hash = key.hashCode();
        return hash % 100;
    }
}

