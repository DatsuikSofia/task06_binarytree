package binaryTree;

public class Main {
    public static void main(String args[]) {
        Realization<Integer, String> map = new Realization<Integer, String>();
        map.langPut(1, "English");
        map.langPut(2, "POland");
        map.langPut(3, "Ukrainian");
        map.langPut(4, "Germany");
        map.langPut(5, "Italian");
        map.langPut(6, "France");
        System.out.println("HashMap [key, value]");

        System.out.println("   1 " + map.langGet(1));
        System.out.println("   2 " + map.langGet(2));
        System.out.println("   3 " + map.langGet(3));
        System.out.println("   4 " + map.langGet(4));
        System.out.println("   5 " + map.langGet(5));
        System.out.println("   6 " + map.langGet(6));
        map.langRemove(3);
        System.out.println("Get third deleted element " + map.langGet(3));
        System.out.println("Get fourth element " + map.langGet(4));
    }
}
