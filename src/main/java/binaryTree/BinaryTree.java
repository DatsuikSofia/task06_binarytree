package binaryTree;

public class BinaryTree<K,V> {
    K key;
    V value;
    BinaryTree<K, V> next = null;

    public BinaryTree<K, V> getNext() {
        return next;
    }

    public void setNext(BinaryTree<K, V> next) {
        this.next = next;
    }

    public BinaryTree(K key, V value) {
        this.key = key;
        this.value = value;
    }

    public K getKey() {
        return key;
    }

    public void setKey(K key) {
        this.key = key;
    }

    public V getValue() {
        return value;
    }

    public void setValue(V value) {
        this.value = value;
    }
}
